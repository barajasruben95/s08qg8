#include <hidef.h>
#include "derivative.h"

unsigned char secuencia[]={0xE0, 0xD0, 0xB0, 0x70, 0XB0, 0xD0};

void delay(void)
{
	SRTISC=0b00000111;		//Habilitar timer utilizando el reloj interno
	/*
	 * SRTISC_RTIS2		SRTISC_RTIS1	 SRTISC_RTIS0
	 * 		0				0					0			Disable RTI		Disable RTI
	 * 		0				0					1			*256				8ms
	 * 		0				1					0			*1024				31ms
	 * 		0				1					1			*2048				64ms
	 * 		1				0					0			*4096				128ms
	 * 		1				0					1			*8192				256ms
	 * 		1				1					0			*16284				512ms
	 * 		1				1					1			*32768				1.024s			 			
	*/			
	do
		{
			asm nop;
		}while(SRTISC_RTIF==0);
	SRTISC_RTIACK=1;				//Apagar bandera
	SRTISC=0;						//Quitar preescaler
}

void main(void)
{
	 unsigned char i=0;
	 PTBDD = 0xF0;				//Poner en salida PTB7 a PTB4
	 //SOPT1 = 0x52;				//Desactivar watchdog	
	 PTAPE_PTAPE2 = 1;			//Activar pull up en PA2
  
	 for(;;)
	 {
		 __RESET_WATCHDOG();
		 //PTBD_PTBD7=~PTBD_PTBD7;
		 //delay();
		 
		 if(PTAD_PTAD2==1)		//SECUENCIA SIN PRESIONAR PUSH BOTTON
		 {
			 //PTBD=secuencia[(i++)%4];
			 //PTBD=secuencia[(i--)%4]; Secuencia para el else
			 PTBD=secuencia[i++];
			 if(i>3) i=0;
			 delay();
		 }
		 else					//SECUENCIA PRESIONANDO EL PUSH BOTTON
		 {
			 //PTBD=secuencia1[i--];
			 //if(i==255) i=3;
			 //delay();
			 PTBD=secuencia[i++];
			 if(i>5) i=0;
			 if(i==255) i=5;
			 delay();
		 }
	 }	
}


/*
 * Mover configuracion al main SRTISC=0b00010111;
 * Ya no existe
 * Dejar en el for solamente el watchdog
*/
/*
interrupt 23 interrupcionISR(void)		//Interruption Service Routine
{
	SRTISC_RTIACK=1;					//Apagar bandera
	PTBD_PTBD6^=1;
}
*/
