// Se�al PWM

#include <hidef.h>
#include "derivative.h"

unsigned char dutyCicle;
unsigned char espejoPin;

interrupt 6 void CH1_ISR(void) //Vector de interrupcion del Timer/PWM
{
	(void) TPMC1SC; //Leer registro de control y estado del canal pero nunca almacenarla puesto que no es util. 1er paso de borrado.
	TPMC1SC_CH1F = 0; //Apagar bandera. 2do paso de borrado.
	if(espejoPin == 0 )
	{
		espejoPin = 1;
		TPMC1V += dutyCicle*10;
	}
	else
	{
		espejoPin = 0;
		TPMC1V += (100 - dutyCicle)*10;
	}
}

void main(void)
{
  EnableInterrupts; //Habilitador global de interrupciones.
  
  dutyCicle = 25;
  
  PTBDD_PTBDD5=1; //Pin B5 como salida. La salida del chanel 1 (TPMCH1) se encuentra ubicado en la posicion del pin B5 de los pines del microcontrolador.
  PTBD_PTBD5=0; //Escribir 0 logico en B5.
  
  espejoPin = 0;
  
  TPMSC = 0b00001010; //Deshabilitar interrupcion del Timer/PWM, fuente de reloj interno y preescalador de 4 (1us).
  TPMC1SC = 0b 01 01 01 00; //Habilitar interrupcion del canal 1 del Timer/PWM, modo output compare toogle output compare.  
  
  TPMC1V = (100 - dutyCicle)*10;
  
  for(;;)
  {
    __RESET_WATCHDOG();
    
  }
}
