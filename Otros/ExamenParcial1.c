// 16us

#include <hidef.h>
#include "derivative.h"

unsigned char frec[] = {62, 312, 625, 1250, 6250, 15625, 31250, 46875};
unsigned char lecturaADC = 0;
unsigned char contadorVueltas = 0;
unsigned char i =0;

interrupt 5 void CH0_ISR(void)
{
	if(contadorVueltas == estimulacion)
	{
		PTAD_PTAD0 = ~PTAD_PTAD0;
		contadorVueltas = 0;
		
		(void) TPMC0SC;
		TPMC0SC_CH0F = 0;
		for(i=0; frecuencias; i++)
		{
			TPMC1V = frec[i];
		}
	}
	contadorVueltas += 1;
}


interrupt 19 ADC_ISR (void)
{
	lecturaADC = ADCRL;
	
	if(lecturaADC > 127)
		 TPMSC = 0b00000000;
	else
		TPMSC = 0b00001110;
	ADCSC1=0b000011111;
}

void main(void)
{
  EnableInterrupts;
  
  PTADD_PTADD0=1; //PTA 0 ser� usado como salida PWM.
  PTAD_PTAD0=0;
  PTBPE = 0b00111111; //Pull-up para los 6 botones
  
  TPMSC = 0b00001110; //Tiempos de 16us.
  TPMC0SC = 0b01010000; //No toogle funcion.
  
  TPMC1V = frec[0]; 
  
  for(;;)
  {
	  frecuencias = PTBD & 0b00000111; //Frecuencias contendr� un valor del 0 al 7.
	  estimulacion = ((PTBD & 0b00111000) >> 3) + 1; //Estimulacion tendr� un valor de 1 hasta 9
	  if(estimulacion > 7)
	  {
		  estimulacion = 7; //Solo valores de 1 a 7
	  }
    __RESET_WATCHDOG();
  }
}
