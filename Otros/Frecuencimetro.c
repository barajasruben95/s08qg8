//Frecuencimetro

#include <hidef.h>
#include "derivative.h"

unsigned int tiempoViejo = 0;

interrupt 6 void CH1_ISR(void) //Vector de interrupcion del Timer/PWM.
{
	unsigned int T;
	unsigned int f;
	(void) TPMC1SC; //Leer registro de control y estado del canal pero nunca almacenarla puesto que no es util. 1er paso de borrado.
	TPMC1SC_CH1F = 0; //Apagar bandera. 2do paso de borrado.
	
	T = TPMC1V - tiempoViejo;
	//f = 1000000/T; //Se divide variable de 32 bits en 16 bits. Se debe crecer variable de 16 bits a 32 bits.
	f = (unsigned int)(1000000/(unsigned long) T);
	tiempoViejo = TPMC1V; 
}

void main(void)
{
  EnableInterrupts; //Habilitar interrupciones globales.
  
  TPMSC = 0b00001010; //Deshabilitar interrupciones del Timer/PWM, fuente de reloj interno y preescalador de 4 (1us).
  TPMC1SC = 0b01000100; //Interrupciones del canal 1 habilitadas, input capture y rising edge.
  
  for(;;)
  {
    __RESET_WATCHDOG(); //Resetea el watchdog
  }
}
