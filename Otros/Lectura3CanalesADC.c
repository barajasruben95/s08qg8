//Lectura de 3 canales ADC

unsigned char temp[3];
unsigned char secuencia[]={0b010000000,0b010000011,0b010000111}

interrupt 23 RTI_ISR (void) //Vector de interrupcion del RTI
{
	SRTISC_RTIACK=1; //Apagar bandera del timer
	ADCSC1=secuencia[i]; //Canal i, conversion sencilla, habilitar interrupcion
	//ADCSC1=0b010000000; //Canal cero, conversion sencilla y habilitar interrupcion	
}

interrupt 19 ADC_ISR (void) //Vector de interrupcion del ADC
{
	temp[i++]=ADCRL; //Lectura del canal i.
	if(i>2)
		i=0;
	ADCSC1=0b000011111; //Detener conversion seleccionando un canal que no existe.
}

void main void
{
	EnableInterrupts; //Interrupciones globales habilitadas
	SRTISC = 0b00010111; // Habilitar interrupciones generadas por el RTI cada 1024ms
	APCTL1_ADPC1=1; //Configurar pin A1 como entrada analógica
	APCTL1_ADPC3=1; //Configurar pin A3 como entrada analógica
	APCTL1_ADPC7=1; //Configurar pin B3 como entrada analógica
		
	for(;;)
	{
		__RESET_WATCHDOG(); //Resetear conteo del watchdog		
	}	
}
