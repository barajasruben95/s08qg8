#include <hidef.h>
#include "derivative.h"

unsigned char secuencia[6] = {0xE0, 0xD0, 0xB0, 0x70, 0XB0, 0xD0};

void delay(void)
{
	SRTISC = 0b00000111;			//Habilitar timer utilizando el reloj interno
	/*
	 * SRTISC_RTIS2		SRTISC_RTIS1	 SRTISC_RTIS0
	 * 		0				0					0			Disable RTI		Disable RTI
	 * 		0				0					1			*256				8ms
	 * 		0				1					0			*1024				31ms
	 * 		0				1					1			*2048				64ms
	 * 		1				0					0			*4096				128ms
	 * 		1				0					1			*8192				256ms
	 * 		1				1					0			*16284				512ms
	 * 		1				1					1			*32768				1.024s			 			
	*/			
	do
		{
			asm nop;
		}while(SRTISC_RTIF == 0);
	SRTISC_RTIACK = 1;				//Apagar bandera
	SRTISC = 0;						//Quitar preescaler
}

void main(void)
{
	 unsigned char i = 0;
	 PTBDD = 0xF0;					//Poner en salida PTB7 a PTB4
	 PTAPE_PTAPE2 = 1;				//Activar pull up en PA2
	 SOPT1 = 0x52;					//Desactivar watchdog
  /*
   * --SOPT1 REGISTER-- 0b1101001u
   * COPE: This write-once bit selects whether the COP watchdog is enabled.
   * 	0: COP watchdog timer disabled.
   * 	1: Watchdog timer enabled (force reset on timeout).
   * COPT: This write-once bit selects the timeout period of the COP. COPT along with COPCLKS in SOPT2 defines the COP timeout period.
   * 	0: Short timeout period selected.
   * 	1: Long timeout period selected.
   * STOPE: This write-once bit is used to enable stop mode. If stop mode is disabled and a user program attempts to execute a STOP instruction, an illegal opcode reset is forced.
   * 	0: Stop mode disabled.
   * 	1: Stop mode enabled.
   * BKGDPE: This write-once bit when set enables the PTA4/ACMPO/BKGD/MS pin to function as BKGD/MS. When clear, the pin functions as one of its output only alternative functions. This pin defaults to the BKGD/MS function following any MCU reset.
   * 	0: PTA4/ACMPO/BKGD/MS pin functions as PTA4 or ACMPO.
   * 	1: PTA4/ACMPO/BKGD/MS pin functions as BKGD/MS.
   * RSTPE: This write-once bit when set enables the PTA5/IRQ'/TCLK/RESET' pin to function as RESET'. When clear, the pin functions as one of its input only alternative functions. This pin defaults to its input-only port function following an MCU POR. When RSTPE is set, an internal pullup device is enabled on RESET.
   * 	0: PTA5/IRQ'/TCLK/RESET' pin functions as PTA5, IRQ, or TCLK.
   * 	1: PTA5/IRQ'/TCLK/RESET' pin functions as RESET.
  */
	 for(;;)
	 {
		 if(PTAD_PTAD2 == 1)			//SECUENCIA SIN PRESIONAR PUSH BOTTON
		 {
			 PTBD = secuencia[i++];
			 if(i>3) i = 0;
			 delay();
		 }
		 else							//SECUENCIA PRESIONANDO EL PUSH BOTTON
		 {
			 PTBD = secuencia[i++];
			 if(i>5) i=0;
			 delay();
		 }
	 }	
}
