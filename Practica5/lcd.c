#include <hidef.h>
#include "derivative.h"
#include "lcd.h"
#include "retardos.h"

#define DB7 PTBD_PTBD7
#define DB6 PTBD_PTBD6
#define DB5 PTBD_PTBD5
#define DB4 PTBD_PTBD4
#define E PTBD_PTBD3
#define RS PTBD_PTBD2

static unsigned char modo4or8 = 1;
unsigned char banderaSeccion = 0;
unsigned char banderaTimer1LCD = 0;
unsigned char banderaTimer2LCD = 0;
unsigned char primerComando = 1;

//**********************************************************
void LCD_Init(void)
{
	//Datos
	PTBDD_PTBDD7 = 1;
	PTBDD_PTBDD6 = 1;
	PTBDD_PTBDD5 = 1;
	PTBDD_PTBDD4 = 1;
	DB7 = 0;
	DB6 = 0;
	DB5 = 0;
	DB4 = 0;
	//Control
	PTBDD_PTBDD3 = 1;
	PTBDD_PTBDD2 = 1;
	E = 0; //Pin E
	RS = 0; //Pin RS
	time(313); //20ms

	//Secuencia de inicializacion
	LCD_Escribe(0b00110000,0);
	time(79); //5ms
	LCD_Escribe(0b00110000,0);
	time(4); //200us
	LCD_Escribe(0b00110000,0);

	//Configuraciones principales
	LCD_4Bits2Lines5x8();
	LCD_IncrementarCursor();
	LCD_CursorON();
	LCD_Clear();
}

//***********************************************************
//Borra toda la pantalla, memoria DDRAM y pone el cursor al comienzo de la linea 1
void LCD_Clear(void)
{
	LCD_Escribe(0b00000001,0);
}

//***********************************************************
//Cursor al principio de la linea 1
void LCD_Home(void)
{
	LCD_Escribe(0b00000010,0);
}

//***********************************************************
//
// _Incrementos 0b00000010
// _Decrementos 0b00000000
//
void LCD_EntryMode(unsigned char incrementDecrement)
{
	LCD_Escribe(0b00000100 | incrementDecrement,0); 
}

//***********************************************************
//
// _DisplayON 0b00000100
// _DisplayOFF 0b00000000
// _CursorON 0b00000010
// _CursorOFF 0b00000000
// _CursorBlinkingON 0b00000001
// _CursorBlinkingOFF 0b00000000
//
void LCD_ControlDisplay(unsigned char displayONOFF, unsigned char cursorONOFF, unsigned char blinkingONOFF)
{
	LCD_Escribe(0b00001000 | displayONOFF | cursorONOFF | blinkingONOFF,0);
}

//***********************************************************
//
// _DisplayShift 0b00001000
// _CursorMove 0b00000000
// _ShiftCursorRigth 0b00000100
// _ShiftCursorLeft 0b00000000
//
void LCD_CursorDisplayShift(unsigned char cursorDisplayShift, unsigned char direction)
{
	LCD_Escribe(0b00010000 | cursorDisplayShift | direction,0);
}

//***********************************************************
//
// _8Bits 0b00010000
// _4Bits 0b00000000
// _2Lines 0b00001000
// _1Line 0b00000000
// _5x10Dots 0b00000100
//_5x8Dots 0b00000000
//
void LCD_FunctionSet(unsigned char dataLength, unsigned char displayLines, unsigned char characterFont)
{
	LCD_Escribe(0b00100000 | dataLength | displayLines | characterFont,0);
}

//***********************************************************
//lineNumber: 11000000 Para linea 2 y 10000000 Para linea 1
void LCD_SetDDRAM(unsigned char posicion, unsigned char lineNumber)
{
	LCD_Escribe((0b10000000 + posicion) | lineNumber,0); //Direccion 00h de la DDRAM mas el valor de la posicion y linea
}

//***********************************************************
//Interfaz de 4Bits, pantalla de 2 lineas, con caracteres de 5 x 7
void LCD_4Bits2Lines5x8(void)
{
	LCD_Escribe(0b00101000,0);
	modo4or8 = 0;
}

//***********************************************************
void LCD_IncrementarCursor(void)
{
	LCD_Escribe(0b00000110,0);
}

//Cursor al principio de la linea 1
void LCD_Linea1(void)
{
	LCD_Escribe(0b10000000,0); //Direccion 00h de la DDRAM
}

//***********************************************************
//Cursor al principio de la linea 2
void LCD_Linea2 (void)
{
	LCD_Escribe(0b11000000,0); //Direccion 40h de la DDRAM
}

//***********************************************************
//Cursor a posicion de la linea 1
void LCD_PosicionLinea1(unsigned char posicion)
{
	LCD_Escribe(0b10000000 + posicion,0); //Direccion 00h de la DDRAM mas el valor de la posicion
}

//***********************************************************
//Cursor a posicion de la linea 2
void LCD_PosicionLinea2(unsigned char posicion)
{
	LCD_Escribe(0b11000000 + posicion,0); //Direccion 40h de la DDRAM mas el valor de la posicion
}

//***********************************************************
//Apagar pantalla
void LCD_OFF(void)
{
	LCD_Escribe(0b00001000,0);
}

//***********************************************************
//Pantalla encendida y cursor encendido
void LCD_CursorON(void)
{
	LCD_Escribe(0b00001110,0);
}

//***********************************************************
//Pantalla encendida y cursor apagado
void LCD_CursorOFF(void)
{
	LCD_Escribe(0b00001100,0);
}

//***********************************************************
//Escritura de datos
void LCD_Write(unsigned char dato)
{
	if(dato - '�' == 0)
		dato = 0b11101110;
	if(dato - '�' == 0)
		dato = 0b11101110; //No existe codigo � en la CGROM de la LCD
	if(dato - '�' == 0)
		dato = 0b11011111;
	LCD_Escribe(dato,1);
}

//***********************************************************
static void LCD_Escribe(unsigned char dato, unsigned char tipo)
{
	if(tipo == 0 )
		RS = 0; //Comando
	else
		RS = 1; //Dato

	E = 0; //Garantizar que la linea E comienza en cero
	E = 1; //Flanco de subida para comenzar a escribir

	if (dato & 0x80)
		DB7 = 1;
	else
		DB7 = 0;

	if (dato & 0x40)
		DB6 = 1;
	else
		DB6 = 0;

	if (dato & 0x20)
		DB5 = 1;
	else
		DB5 = 0;

	if (dato & 0x10)
		DB4 = 1;
	else
		DB4 = 0;

	E=0; //Capturar dato en el LCD

	if(tipo == 0)
		time(32); //Para un comando, 2ms
	else
		time(32); //Para un dato, 50us

	if (modo4or8 == 0) //Revisar si son datos de 4 o 8 bits
	{
		E=1; //Flanco de subida nuevamente para capturar la parte restante en modo 4 bits

		if (dato & 0x08)
			DB7 = 1;
		else
			DB7 = 0;

		if (dato & 0x04)
			DB6 = 1;
		else
			DB6 = 0;

		if (dato & 0x02)
			DB5 = 1;
		else
			DB5 = 0;

		if (dato & 0x01)
			DB4 = 1;
		else
			DB4 = 0;

		E=0; //Capturar dato

		if(tipo == 0)
			time(32); //Para un comando, 2ms
		else
			time(32); //Para un dato, 50us
	}
}

void LCD_Berenjena(unsigned char data)
{	
	RS = 1; //Dato

	E = 0; //Garantizar que la linea E comienza en cero
	E = 1; //Flanco de subida para comenzar a escribir

	if (data & 0x80)
		DB7 = 1;
	else
		DB7 = 0;

	if (data & 0x40)
		DB6 = 1;
	else
		DB6 = 0;

	if (data & 0x20)
		DB5 = 1;
	else
		DB5 = 0;

	if (data & 0x10)
		DB4 = 1;
	else
		DB4 = 0;

	E=0; //Capturar dato en el LCD

	if(banderaSeccion == 0)
		banderaTimer1LCD = 1;
	else
		banderaTimer2LCD = 1;
	MTIMSC = 0b01000000; //Con interrupcion, cuenta desde cero, cuenta activa
	primerComando = primerComando ^ 1;
}
