#include <hidef.h>
#include "derivative.h"
#include "gpio.h"

void GPIO_Init(void)
{
	APCTL1 = 0;
	
	//Pines del motor a pasos
	PTADD = 0b11111111;
	
	//Comenzar con bobinas desenergizadas
	PTAD = 0b00000000;
}

void GPIO_Write(unsigned char value)
{
	value = value >> 4;
	PTAD = value; //Escribir sobre las bobinas del motor
}
