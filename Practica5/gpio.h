#ifndef GPIO_H_
#define GPIO_H_

void GPIO_Init(void);
void GPIO_Write(unsigned char value);

#endif
