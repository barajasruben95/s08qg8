#ifndef UART_H_
#define UART_H_

//BaudRate = BusCLK/(SBR12:SBR0 * 16)
//SBR12:SBR0 = BusCLK/(BaudRate * 16)
//Error: (BaudRateIdeal-BaudRateReal/BaudRateIdeal)*100

//Para una velocidad de 4MHz
#define tasa110bps 2273 //Error: 0.01%. Funciona.
#define tasa300bps 833 //Error:  0.04%. Funciona.
#define tasa1200bps 208 //Error:  0.16%. Funciona.
#define tasa2400bps 104 //Error:  0.16%. Funciona.
#define tasa4800bps 52 //Error:  0.16%. Funciona.
#define tasa9600bps 26 //Error: 0.16% . Funciona.
#define tasa19200bps 13 //Error: 0.16%. Funciona.
#define tasa38400bps 6 //Error: 8.5%. No funciona.
#define tasa57600bps 4 //Error: 8.5%. No funciona.
#define tasa115200bps 2 //Error: 8.5%. No funciona.
#define tasa230400bps 1 //Error: 8.5%. No funciona.

void UART_Enable(void);
void UART_Disable(void);

void UART_TX_Enable(void);
void UART_TX_Disable(void);
void UART_RX_Enable(void);
void UART_RX_Disable(void);

void UART_InterruptTX_Enable(void);
void UART_InterruptTX_Disable(void);
void UART_InterruptRX_Enable(void);
void UART_InterruptRX_Disable(void);

unsigned char UART_DataReady(void);

void UART_BaudRate(unsigned char baudRate);

unsigned char UART_Read(void);
void UART_Write(unsigned char data);

#endif
