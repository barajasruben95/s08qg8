#include <hidef.h>
#include "derivative.h"
#include "retardos.h"

static unsigned int i;

void time(unsigned int delay)
{
	MTIMCLK = 0b00001000; //Preescalador de 256 (64us)
	MTIMSC = 0b00100000; //Sin interrupcion, cuenta desde cero, cuenta activa
	
	do
	{
		if(delay > 255)
		{
			MTIMMOD = 255;
			i = 1;
		}
		else
		{
			MTIMMOD = (char)delay;
			i = 0;
		}
		do
		{
			asm nop;
		}while(MTIMSC_TOF == 0);
		MTIMSC_TRST = 1; //Apagar bandera
		
		if(i == 1)
			delay = delay - 255;
		else
			delay = 0;
	}while(delay != 0);
	MTIMSC_TSTP = 1;
}
