#ifndef TIMERPWM_H_
#define TIMERPWM_H_

void TimerPWM_Init(void);
void TimerPWM_Disable(void);
void TimerPWM_Start(unsigned int delay);
void TimerPWM_CleanFlag(void);


#endif
