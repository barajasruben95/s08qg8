#include <hidef.h>
#include "derivative.h"
#include "gpio.h"

void GPIO_Init(void)
{
	PTBDD = 0x0F; //Poner en salida PTB4 a PTB0 (pines del motor a pasos)
	PTAPE_PTAPE2 = 1; //Activar pull up en PA2 (donde se pondra un push botton)
}

void GPIO_Write(unsigned char value)
{
	PTBD = value; //Escribir sobre el puerto B
}

unsigned char GPIO_Read(void)
{
	return PTAD_PTAD2; // Leer valor del push botton
}
