/*
 * CARACTERISTICAS DEL MOTOR A PASOS UTILIZADO
 *  - Voltaje: 20v m�ximo.
 *  - Grado por paso: 7.5
 *  - Secuencia: rojo, azul, cafe, amarillo.
 *  - Com�n: blanco 
 */

#include <hidef.h>
#include "derivative.h"
#include "adc.h"
#include "gpio.h"
#include "timerPWM.h"

unsigned char secuencia[] = { 0b00001000, 0b00001100, 0b00000100, 0b00000110, 0b00000010, 0b00000011, 0b00000001, 0b00001001 };
unsigned char datoADC = 0;
unsigned char ejecutar = 0;
unsigned char banderaTimer = 0;

void RTC_Init(void);

interrupt 7 void TimerPWM_ISR(void) //Vector de interrupcion del Timer/PWM
{
	ejecutar = 1; //Si entra a la interrupcion quiere decir que ya pas� el tiempo indicado para cambiar de secuencia
	TimerPWM_CleanFlag(); //Limpiar bandera del timer
	ADC_Start(); //Comenzar conversion ADC
}

interrupt 19 void ADC_ISR(void) //Vector de interrupcion del ADC
{
	banderaTimer = 1;
	datoADC = ADC_Read(); //Leer conversion del ADC
	ADC_Stop(); //Detener operacion del ADC
}

interrupt 23 void RTI_ISR(void) //Vector de interrupcion del RTI
{
	asm
	{
		//Apagar bandera del timer: SRTISC_RTIACK=1
		LDHX #0x1808 //El registro X contiene el valor de SRTISC
		LDA ,X //Cargar el registro A con el valor del registro X
		ORA #0x40 //Aplicar operacion A | 0x40. SRTISC_RTIACK=1. Operacion OR para solo cambiar un bit a uno.
		STA ,X //Cargar del valor del registro A en el registro apuntado por X (0x1808)

		//Toogle pin
		LDHX #0x0000 //El registro X contiene el valor de PTA
		LDA ,X //Cargar el registro A con el valor del registro X
		EOR #0x02 //Aplicar operacion A XOR 0x02. Toogle pin.
		STA ,X //Cargar del valor del registro A en el registro apuntado por X (0x1808)
	}

	//SRTISC_RTIACK=1;
	//PTAD_PTAD1 = ~PTAD_PTAD1; 
}

void main(void)
{
	unsigned char i = 0;

	SOPT1 = 0x52; //Desactivar watchdog

	GPIO_Init();
	TimerPWM_Init();
	ADC_Init();
	ADC_Start();
	RTC_Init();

	PTADD_PTADD1 = 1; //Poner en salida el pin A1.
	PTAD_PTAD1 = 1; //Comenzar con LED encendido

	EnableInterrupts;

	for (;;) 
	{
		if (ejecutar == 1)
		{
			ejecutar = 0;
			if (GPIO_Read() == 1) //SECUENCIA SIN PRESIONAR PUSH BOTTON
			{
				GPIO_Write(secuencia[(i++) % 8]);
			}
			else //SECUENCIA PRESIONANDO EL PUSH BOTTON
			{
				GPIO_Write(secuencia[(i--) % 8]);
			}
		}
		else
			asm nop;

		if (banderaTimer == 1)
		{
			banderaTimer = 0;
			TimerPWM_Start((int) (datoADC * 49 + 750)); //Enviarle al timer el tiempo ajustado para incrementos/decrementos graduales
		}
	}
}

void RTC_Init(void)
{
	SRTISC = 0b00010111;
	/*
	 // Habilitar interrupciones generadas por el RTI cada 1024ms
	 asm
	 {
	 LDA #0x17; //Cargar el registro A con 0b00010111
	 STA 0x1808; //Mover el valor del registro A al registro ubicado en la direccion de memoria 0x1808 (Registro SRTISC).  
	 }
	 */
}
