#ifndef ADC_H_
#define ADC_H_

void ADC_Init(void);
void ADC_Disable(void);
void ADC_Start(void);
void ADC_Stop(void);
unsigned char ADC_Read(void);

#endif
