#include <hidef.h>
#include "derivative.h"
#include "adc.h"

void ADC_Init(void)
{
	APCTL1_ADPC0 = 1; //Configurar como entrada anal�gica el canal 0 (PTA0)
	ADCCFG = 0b00001000; //Configurar ADC en modo 10 bits
}

void ADC_Disable(void)
{
	APCTL1_ADPC0 = 0; //Configurar como puerto de entrada/salida el pin PTA0
}

void ADC_Start(void)
{
	ADCSC1 = 0b01000000;  //Iniciar conversion ADC modo no continuo por el canal 0 con interrupciones.
}

void ADC_Stop(void)
{
	ADCSC1=0b00001111;  //Desactivar conversiones del ADC.
}

unsigned int ADC_Read(void)
{
	return ADCR; //Leer el dato del ADC. Se apagar� la bandera tambi�n.
}
