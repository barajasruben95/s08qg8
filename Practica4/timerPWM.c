#include <hidef.h>
#include "derivative.h"
#include "timerPWM.h"

void TimerPWM_Init(void)
{
	TPMSC = 0b01001110; //Interrupciones habilitadas, fuente de reloj interna y preescalador de 64 (16us)
}

void TimerPWM_Disable(void)
{
	TPMSC = 0; //Deshabilitar timer completamente
}

void TimerPWM_EnableInterrupt(void)
{
	TPMSC_TOIE = 1;
}

void TimerPWM_DisableInterrupt(void)
{
	TPMSC_TOIE = 0;
}

void TimerPWM_Start(unsigned int delay)
{
	TPMMOD = delay; //Cargar el registro de comparacion del timer
	TPMCNT = 0; //Forzar cuenta desde cero
}

void TimerPWM_CleanFlag(void)
{
	(void) TPMSC; //Leer variable pero nunca almacenarla puesto que no es util. 1er paso de borrado.
	TPMSC_TOF = 0; //Apagar bandera. 2do paso de borrado.
}
