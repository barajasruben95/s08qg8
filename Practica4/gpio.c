#include <hidef.h>
#include "derivative.h"
#include "gpio.h"

void GPIO_Init(void)
{
	PTBDD = 0xF0; // Poner en salida PTB7 a PTB4 (pines del motor a pasos)
	PTBD = 0x00; // Comenzar con bobinas del motor desenergizadas
}

void GPIO_InitLED(void)
{
	PTBDD_PTBDD3 = 1; //Poner en salida el pin B3.
	PTBD_PTBD3 = 1; //Comenzar con LED encendido
}

void GPIO_Write(unsigned char value)
{
	PTBD = value; //Escribir sobre el puerto B
}

void GPIO_ToogleLED(void)
{
	PTBD_PTBD3 = ~PTBD_PTBD3;
}

void GPIO_LED_Init()
{
	PTBDD_PTBDD3 = 1; //Poner en salida PTB3 para el LED
	PTBD_PTBD3 = 1; //Comenzar con el LED apagado
}
