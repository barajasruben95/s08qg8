/*
 * CARACTERISTICAS DEL MOTOR A PASOS UTILIZADO
 *  - Voltaje: 20v m�ximo.
 *  - Grado por paso: 7.5
 *  - Secuencia: rojo, azul, cafe, amarillo.
 *  - Com�n: blanco 
 */

/*
 * PINES DE LA QG8 PARA EL MOTOR A PASOS
 * PTB7 = 29
 * PTB5 = 27
 * PTB5 = 23
 * PTB4 = 19
 */

#include <hidef.h>
#include "derivative.h"
#include "adc.h"
#include "gpio.h"
#include "rti.h"
#include "timerPWM.h"
#include "uart.h"

unsigned char secuenciaMotor[] = { 0b10000000, 0b11000000, 0b01000000, 0b01100000, 0b00100000, 0b00110000, 0b00010000, 0b10010000 };

unsigned char enviarTemp[] = ("000.0 C\n\r");
unsigned char enviarOverTemp[] = ("000.0 C OVERTEMPERATURE DETECTED\n\r");
unsigned char done[] = ("DONE\n\r");
unsigned char comando[17];

unsigned int datoADC = 0;
unsigned char datoUART = 0;
unsigned int numero = 0; //Contiene el valor numerico proveniente del comando correcto
unsigned int auxiliarNumero = 0; //Variable para apoyar en los comandos a la variable numero

unsigned int tempMaxima = 500; //Temperatura maxima para apagar el motor y enviar Overtemperature
unsigned int velocidadMotor = 651; //Velocidad del motor
unsigned int pasosMotor = 0; //Cantidad de pasos que debe dar el motor en modo STEPCW:ggg
unsigned char tiempoActualizacion = 5; //Tiempo de envio de la actualizacion de temperatura 

unsigned char i = 0; //Contador para enviar la temperatura
unsigned char j = 0; //Contador para enviar done
unsigned char k = 0; //Contador para almacenar dato de entrada
unsigned char l = 0; //Contador para la secuencia del motor
unsigned int m = 0; //Contador para la cantidad de pasos a dar en el modo STEPCW:ggg
unsigned char n = 0; //Contador para limpiar el buffer de entrada

//Banderas
unsigned char banderaActualizacion = 0;
unsigned char banderaComienzaADC = 1;
unsigned char banderaFinalizaADC = 0;
unsigned char banderaEnter = 0;
unsigned char banderaDatoRX = 0;
unsigned char banderaEnviarDone = 0;

unsigned char banderaMotorONOFF = 0; //MOTOR:ON o MOTOR:OFF
unsigned char banderaMotorModoContinuo = 0; //Para habilitar/deshabilitar el motor cuando excede o no el limite de temperatura
unsigned char banderaPasosMotor  = 0; //Dice si es momento de dar un paso. Depende del tiempo colocado en el timerPWM
unsigned char banderaGiroMotor = 0; //Giro a la derecha o a la izquierda
unsigned char banderaComienzaGiro = 0; //Conocer cuando comenzar y terminar la cantidad de grados a girar
unsigned char banderaTipoEnvio = 0; //Bandera que contiene el dato a enviar por el serial: DONE, Overtemperature detected y temperatura
unsigned char banderaEnviarDato = 0; //Bandera para saber cuando es momento de enviar un dato por el serial.

//Funciones
void convertirDatoADC(void);
void enviarDone(void);
void inicializaComando(void);

//***********************************************************
interrupt 7 void TimerPWM_ISR(void) //Interrupcion del Timer/PWM
{
	TimerPWM_CleanFlag();
	banderaPasosMotor = 1;
	if(banderaComienzaGiro == 1)
		pasosMotor--;
	if(pasosMotor > 267)
	{
		banderaComienzaGiro = 0;
		pasosMotor = 0;
	}
}

interrupt 15 void RX_ISR(void) //Interrupcion del UART RX
{
	datoUART = UART_Read();
	banderaDatoRX = 1;
}

interrupt 16 void TX_ISR(void) //Interrupcion del UART TX
{
	if(banderaTipoEnvio == 1)
	{
		UART_Write(enviarOverTemp[i++]);
		if(enviarOverTemp[i] == 0)
		{
			UART_InterruptTX_Disable();
			i = 0;
			banderaTipoEnvio = 0;
		}
	}
	else if(banderaTipoEnvio == 2)
	{
		UART_Write(enviarTemp[i++]);
		if(enviarTemp[i] == 0)
		{
			UART_InterruptTX_Disable();
			i = 0;
			banderaTipoEnvio = 0;
		}
	}
	else if(banderaTipoEnvio == 3)
	{
		UART_Write(done[j++]);
		if(done[j] == 0)
		{
			banderaEnviarDone = 0;
			UART_InterruptTX_Disable();
			j = 0;
			banderaTipoEnvio = 0;
		}
	}
	else
		asm nop;
}

interrupt 19 void ADC_ISR(void) //Interrupcion del ADC
{
	datoADC = ADC_Read();
	ADC_Stop();
	banderaFinalizaADC = 1;
}

interrupt 23 void RTI_ISR(void) //Interrupcion del RTI
{
	RTI_ClearFlag();
	GPIO_ToogleLED(); //Sacarla de la interrupcion
	banderaActualizacion++;
}

//***********************************************************
void main(void)
{
	SOPT1 = 0x52; //Desactivar watchdog

	GPIO_Init();
	GPIO_InitLED();
	ADC_Init();
	TimerPWM_Init();
	UART_BaudRate(tasa9600bps);
	UART_Enable();
	UART_InterruptRX_Enable();
	RTI_Init();

	TimerPWM_Start(651); //Comenzar con una velocidad predefinida. 60 revoluciones por segundo

	EnableInterrupts;

	for (;;)
	{
		//Control de envio de datos por serial
		if(banderaEnviarDato == 1)
		{
			banderaEnviarDato = 0;
			if(banderaEnviarDone == 1)
				banderaTipoEnvio = 3;
			else
			{
				if(datoADC > tempMaxima)
				{
					banderaMotorONOFF = 1; //Apagar motor si excede la temperatura
					banderaTipoEnvio = 1;
				}
				else
				{
					if(banderaMotorModoContinuo == 0)
						banderaMotorONOFF = 0; //Mantener motor encendido si no excede la temperatura y si debe estar encendido (COMANDO MOTOR:ON)
					else
						banderaMotorONOFF = 1; //Apagar motor si no debe estar encendido (COMANDO MOTOR:OFF)
					banderaTipoEnvio = 2;
				}
			}
			UART_InterruptTX_Enable();
		}

		//Dar pasos del motor
		if(banderaPasosMotor == 1)
		{
			banderaPasosMotor = 0;
			if(banderaMotorONOFF == 0)
			{
				//SECUENCIAS DEL MOTOR A PASOS
				if (banderaGiroMotor == 0)
					GPIO_Write(secuenciaMotor[(l++) % 8]);
				else
					GPIO_Write(secuenciaMotor[(l--) % 8]);
			}
			else
			{
				if(banderaComienzaGiro == 1)
				{
					//SECUENCIAS DEL MOTOR A PASOS
					if (banderaGiroMotor == 0)
						GPIO_Write(secuenciaMotor[(l++) % 8]);
					else
						GPIO_Write(secuenciaMotor[(l--) % 8]);
				}
			}
		}

		//Actualizacion de temperatura
		if(banderaActualizacion == tiempoActualizacion)
		{
			if(banderaComienzaADC == 1)
			{
				banderaComienzaADC = 0;
				ADC_Start();
			}

			if(banderaFinalizaADC == 1)
			{
				banderaActualizacion = 0;
				banderaFinalizaADC = 0;
				banderaComienzaADC = 1;
				convertirDatoADC();
				if(datoADC > tempMaxima)
					UART_Write(enviarOverTemp[i++]);
				else
					UART_Write(enviarTemp[i++]);
				banderaEnviarDato = 1;
			}
		}

		//Lectura del dato de entrada
		if(banderaDatoRX == 1)
		{
			banderaDatoRX = 0;
			if((datoUART > '/') && (datoUART < ':')) //Cuando sean datos numericos, guardarlos en una variable aparte. Sino, guardarlos en el buffer de entrada
				auxiliarNumero = (auxiliarNumero * 10) + datoUART - '0';
			else
				comando[k] = datoUART;
			k++;
			if(datoUART == 13) //Revisar si ya ha llegado el comando ENTER
			{
				k = 0;
				banderaEnter = 1;
				numero = auxiliarNumero;
				auxiliarNumero = 0;
			}
		}

		//Comando RPS:nn.n
		if(		comando[1] == 'R' && comando[2] == 'P' && comando[3] == 'S' && comando[4] == ':' && comando[5] == 0 && 
				comando[6] == 0 && comando[7] == '.' && comando[8] == 0 && comando[9] == 13 && comando[10] == 0 &&
				comando[11] == 0 && comando[12] == 0 && comando[13] == 0 && comando[14] == 0 && comando[15] == 0 && comando[16] == 0)
		{
			enviarDone();
			inicializaComando();
			if(numero > 0 && numero < 6) //Para 0.1, 0.2, 0.3, 0.4 y 0.5 los valores a cargar en el timer son superiores a 16 bits
				numero = 6;
			if(numero == 0)
			{
				banderaMotorModoContinuo = 1;
				banderaMotorONOFF = 1; //Apagar motor si RPS:00.0
			}
			else
				TimerPWM_Start((int)(390620/numero));
		}

		//Comando DIR:CW
		if(		comando[1] == 'D' && comando[2] == 'I' && comando[3] == 'R' && comando[4] == ':' && comando[5] == 'C' && 
				comando[6] == 'W' && comando[7] == 13 && comando[8] == 0 && comando[9] == 0 && comando[10] == 0 &&
				comando[11] == 0 && comando[12] == 0 && comando[13] == 0 && comando[14] == 0 && comando[15] == 0 && comando[16] == 0)
		{
			enviarDone();
			inicializaComando();
			banderaGiroMotor = 0;
		}

		//Comando DIR:CCW
		if(		comando[1] == 'D' && comando[2] == 'I' && comando[3] == 'R' && comando[4] == ':' && comando[5] == 'C' &&
				comando[6] == 'C' && comando[7] == 'W' && comando[8] == 13 && comando[9] == 0 && comando[10] == 0 &&
				comando[11] == 0 && comando[12] == 0 && comando[13] == 0 && comando[14] == 0 && comando[15] == 0 && comando[16] == 0)
		{
			enviarDone();
			inicializaComando();
			banderaGiroMotor = 1;
		}

		//Comando MOTOR:ON
		if(		comando[1] == 'M' && comando[2] == 'O' && comando[3] == 'T' && comando[4] == 'O' && comando[5] == 'R' && 
				comando[6] == ':' && comando[7] == 'O' && comando[8] == 'N' && comando[9] ==  13 && comando[10] == 0 &&
				comando[11] == 0 && comando[12] == 0 && comando[13] == 0 && comando[14] == 0 && comando[15] == 0 && comando[16] == 0)
		{
			enviarDone();
			inicializaComando();
			banderaMotorModoContinuo = 0; //Modo continuo habilitado
			banderaMotorONOFF = 0; //Encender motor
		}

		//Comando MOTOR:OFF
		if(		comando[1] == 'M' && comando[2] == 'O' && comando[3] == 'T' && comando[4] == 'O' && comando[5] == 'R' && 
				comando[6] == ':' && comando[7] == 'O' && comando[8] == 'F' && comando[9] == 'F' &&comando[10] == 13 &&
				comando[11] == 0 &&comando[12] == 0 && comando[13] == 0 && comando[14] == 0 && comando[15] == 0 && comando[16] == 0)
		{
			enviarDone();
			inicializaComando();
			banderaMotorModoContinuo = 1; //Modo continuo deshabilitado
			banderaMotorONOFF = 1; //Apagar motor
		}

		//Comando STEPW:ggg
		if(		comando[1] == 'S' && comando[2] == 'T' && comando[3] == 'E' && comando[4] == 'P' && comando[5] == 'C' && 
				comando[6] == 'W' && comando[7] ==  ':' && comando[8] ==  0 && comando[9] ==  0 && comando[10] == 0 &&
				comando[11] == 13 && comando[12] == 0 && comando[13] == 0 && comando[14] == 0 && comando[15] == 0 && comando[16] == 0)
		{
			if(banderaMotorModoContinuo == 1) //Primero revisar si esta en modo no continuo para poder ejecutar la instruccion
			{
				enviarDone();
				inicializaComando();
				pasosMotor = 0;
				numero *= 10;
				for(m = 0; m < numero; m += 37) //Encontrar la cantidad de pasos que el motor debe dar para alcanzar los grados deseados
					pasosMotor++;
				banderaComienzaGiro = 1;
			}
		}

		//Comando TEMPLIMIT:ttt.t
		if(		comando[1] == 'T' && comando[2] == 'E' && comando[3] == 'M' && comando[4] == 'P' && comando[5] == 'L' && 
				comando[6] == 'I' && comando[7] == 'M' && comando[8] == 'I' && comando[9] == 'T' && comando[10] == ':' &&
				comando[11] == 0 && comando[12] == 0 && comando[13] == 0 && comando[14] == '.' && comando[15] == 0 && comando[16] == 13)
		{
			enviarDone();
			inicializaComando();
			numero = (numero - numero % 10) / 10; //Eliminar el ultimo digito porque no es importante
			tempMaxima = numero;
		}

		if(banderaEnter == 1)
		{
			banderaEnter = 0;
			inicializaComando();
		}
	}
}

//***********************************************************
void convertirDatoADC(void)
{
	if(datoADC > 999) //Solo permitir valores m�ximos de 000 a 999 para el limite de temperatura
		datoADC = 999;

	enviarTemp[2] = enviarOverTemp[2] = (datoADC % 10) + '0'; //Acomodar el primer digito de la temperatura para ser enviado
	enviarTemp[1] = enviarOverTemp[1] = ((datoADC % 100 - datoADC % 10) / 10) + '0'; //Acomodar el segundo digito de la temperatura para ser enviado
	enviarTemp[0] = enviarOverTemp[0] = ((datoADC - datoADC % 100) / 100) + '0'; //Acomodar el tercer digito de la temperatura para ser enviado
}

void enviarDone(void)
{
	UART_Write(done[j++]);
	banderaEnviarDone = 1;
	banderaEnviarDato = 1;
}

void inicializaComando(void)
{
	//Una vez llegado el comando y analizado, limpiar el buffer de entrada para la llegada de uno nuevo
	for(n = 0; n < 17; n++)
		comando[n] = 0;
}
