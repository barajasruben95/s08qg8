#ifndef GPIO_H_
#define GPIO_H_

void GPIO_Init(void);
unsigned char GPIO_Read(void);
void GPIO_Write(unsigned char value);

#endif
