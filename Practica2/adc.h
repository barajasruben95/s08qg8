#ifndef ADC_H_
#define ADC_H_

void ADC_Init(void);
unsigned char ADC_Read(void);

#endif
