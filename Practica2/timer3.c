#include <hidef.h>
#include "derivative.h"
#include "timer3.h"

void Timer3_Init(void)
{
	TPMSC = 0b00001100; //Interrupciones deshabilitadas, fuente de reloj interna y preescalador de 16 (4us)
}

void Timer3_Wait(unsigned int retardo)
{
	TPMMOD = retardo;
	TPMCNT = 0; //Comenzar cuenta en cero
	do
	{
		asm nop;
	} while (TPMSC_TOF == 0); //Esperar hasta que se haya terminado el conteo.
	(void) TPMSC; //Leer variable pero nunca almacenarla puesto que no es util. 1er paso de borrado.
	TPMSC_TOF = 0; //Apagar bandera. 2do paso de borrado.
}
