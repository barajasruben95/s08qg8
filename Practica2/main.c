/*
 * CARACTERISTICAS DEL MOTOR A PASOS UTILIZADO
 *  - Voltaje: 20v m�ximo.
 *  - Grado por paso: 7.5
 *  - Secuencia: rojo, azul, cafe, amarillo.
 *  - Com�n: blanco 
 */

#include <hidef.h>
#include "derivative.h"
#include "adc.h"
#include "gpio.h"
#include "timer3.h"

unsigned char secuencia[] = { 0b00001000, 0b00001100, 0b00000100, 0b00000110, 0b00000010, 0b00000011, 0b00000001, 0b00001001 };
unsigned int retardo=0;

void main(void)
{
	unsigned char i = 0;

	SOPT1 = 0x52; //Desactivar watchdog
	GPIO_Init();
	ADC_Init();
	Timer3_Init();
	
	for (;;)
	{
		retardo = ADC_Read()*48 + 750;
		
		if(GPIO_Read() == 1) //SECUENCIA SIN PRESIONAR PUSH BOTTON
		{
			if (i == 8)
				i = 0;
			GPIO_Write(secuencia[i++]);
		}
		else //SECUENCIA PRESIONANDO EL PUSH BOTTON
		{
			if (i == 255)
				i = 7;
			GPIO_Write(secuencia[i--]);
		}
		Timer3_Wait(retardo);
	}
}
