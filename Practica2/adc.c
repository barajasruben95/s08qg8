#include <hidef.h>
#include "derivative.h"
#include "adc.h"

unsigned char datoADC = 0;

void ADC_Init(void)
{
	APCTL1_ADPC0 = 1; //Configurar como entrada anal�gica el canal 0 (PTA0)
}

unsigned char ADC_Read(void)
{
	ADCSC1 = 0x00; //Iniciar conversion ADC modo no continuo por el canal 0 sin interrupciones.
	do
	{
		asm nop;
	} while (ADCSC1_COCO == 0);	//Esperar hasta que la conversion est� lista.
	datoADC = ADCRL; //Leer el dato del ADC. Se apagar� la bandera tambi�n.
	return datoADC;
}
