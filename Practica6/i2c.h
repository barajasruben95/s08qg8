#ifndef I2C_H_
#define I2C_H_

void I2C_Init(void);
void I2C_Write(unsigned char dato);
void I2C_Stop(void);
void I2C_ClearFlag(void);

#endif
