#include <hidef.h>
#include "derivative.h"
#include "i2c.h"
#include "i2c_lcd.h"

unsigned char estadoActual;
unsigned char modo4Bits = 0;
unsigned char direccion;

//***********************************************************
void I2C_LCD_Init(unsigned char address)
{
	direccion = address << 1;
	I2C_Init(); //Inicializar el modulo I2C del microcontrolador
	
	//Comenzar con el modulo PCF8574 apagado y con el LCD encendido
	I2C_Write(direccion);
	I2C_Write(0x08);
	estadoActual = 0x08;
	I2C_Stop();

	//Inicializar pantalla I2C_LCD
	I2C_LCD_Inicializar();
}

//***********************************************************
//
// _Incrementos 0b00000010
// _Decrementos 0b00000000
//
void I2C_LCD_EntryMode(unsigned char incrementDecrement)
{
	I2C_LCD_Escribir(0b00000100 | incrementDecrement,0);
}

//***********************************************************
//
// _DisplayON 0b00000100
// _DisplayOFF 0b00000000
// _CursorON 0b00000010
// _CursorOFF 0b00000000
// _CursorBlinkingON 0b00000001
// _CursorBlinkingOFF 0b00000000
//
void I2C_LCD_ControlDisplay(unsigned char displayONOFF, unsigned char cursorONOFF, unsigned char blinkingONOFF)
{
	I2C_LCD_Escribir(0b00001000 | displayONOFF | cursorONOFF | blinkingONOFF,0);
}

//***********************************************************
//
// _DisplayShift 0b00001000
// _CursorMove 0b00000000
// _ShiftCursorRigth 0b00000100
// _ShiftCursorLeft 0b00000000
//
void I2C_LCD_CursorDisplayShift(unsigned char cursorDisplayShift, unsigned char direction)
{
	I2C_LCD_Escribir(0b00010000 | cursorDisplayShift | direction,0);
}

//***********************************************************
//
// _8Bits 0b00010000
// _4Bits 0b00000000
// _2Lines 0b00001000
// _1Line 0b00000000
// _5x10Dots 0b00000100
//_5x8Dots 0b00000000
//
void I2C_LCD_FunctionSet(unsigned char dataLength, unsigned char displayLines, unsigned char characterFont)
{
	I2C_LCD_Escribir(0b00100000 | dataLength | displayLines | characterFont,0);
}

//***********************************************************
//#define _Linea1 0b10000000
//#define _Linea2 0b11000000
//***********************************************************
void I2C_LCD_SetDDRAM(unsigned char posicion, unsigned char lineNumber)
{
	I2C_LCD_Escribir((0b10000000 + posicion) | lineNumber,0); //Direccion 00h de la DDRAM mas el valor de la posicion y linea
}

//***********************************************************
//Borra toda la pantalla, memoria DDRAM y pone el cursor al comienzo de la linea 1
void I2C_LCD_Clear(void)
{
	I2C_LCD_Escribir(0b00000001,0);
}

//***********************************************************
//Cursor al principio de la linea 1
void I2C_LCD_Home(void)
{
	I2C_LCD_Escribir(0b00000010,0);
}

//***********************************************************
//Interfaz de 4Bits, pantalla de 2 lineas, con caracteres de 5 x 7
void I2C_LCD_4Bits2Lines5x8(void)
{
	I2C_LCD_Escribir(0b00101000,0);
	modo4Bits = 1;
}

//***********************************************************
void I2C_LCD_IncrementarCursor(void)
{
	I2C_LCD_Escribir(0b00000110,0);
}

//Cursor al principio de la linea 1
void I2C_LCD_Linea1(void)
{
	I2C_LCD_Escribir(0b10000000,0); //Direccion 00h de la DDRAM
}

//***********************************************************
//Cursor al principio de la linea 2
void I2C_LCD_Linea2 (void)
{
	I2C_LCD_Escribir(0b11000000,0); //Direccion 40h de la DDRAM
}

//***********************************************************
//Cursor a posicion de la linea 1
void I2C_LCD_PosicionLinea1(unsigned char posicion)
{
	I2C_LCD_Escribir(0b10000000 + posicion,0); //Direccion 00h de la DDRAM mas el valor de la posicion
}

//***********************************************************
//Cursor a posicion de la linea 2
void I2C_LCD_PosicionLinea2(unsigned char posicion)
{
	I2C_LCD_Escribir(0b11000000 + posicion,0); //Direccion 40h de la DDRAM mas el valor de la posicion
}

//***********************************************************
//Apagar pantalla
void I2C_LCD_OFF(void)
{
	I2C_LCD_Escribir(0b00001000,0);
}

//***********************************************************
//Pantalla encendida y cursor encendido
void I2C_LCD_CursorON(void)
{
	I2C_LCD_Escribir(0b00001110,0);
}

//***********************************************************
//Pantalla encendida y cursor apagado
void I2C_LCD_CursorOFF(void)
{
	I2C_LCD_Escribir(0b00001100,0);
}

//***********************************************************
//Escribir character
void I2C_LCD_Write(unsigned char character)
{
	if(character - '�' == 0)
		character = 0b11101110;
	if(character - '�' == 0)
		character = 0b11101110; //No existe codigo � en la CGROM de la LCD
	if(character - '�' == 0)
		character = 0b11011111;
	I2C_LCD_Escribir(character,1);
}

//***********************************************************
static void I2C_LCD_Inicializar(void)
{
	delay_ms(20);
	I2C_LCD_Escribir(0b00110000,0);
	delay_ms(5);
	I2C_LCD_Escribir(0b00110000,0);
	delay_us(200);
	I2C_LCD_Escribir(0b00110000,0);

	//Aqui las configuraciones principales
	I2C_LCD_4Bits2Lines5x8();
	I2C_LCD_IncrementarCursor();
	I2C_LCD_CursorON();
	I2C_LCD_Clear();
}

//***********************************************************
static void I2C_LCD_Escribir(unsigned char dato, unsigned char tipo)
{
	estadoActual = 0x08;
	I2C_Write(direccion);

	if(tipo == 0)
		I2C_Write(estadoActual); //Comando
	else
	{
		estadoActual = 0b00000001 | estadoActual;
		I2C_Write(estadoActual); //Dato
	}

	estadoActual = 0b00000100 | estadoActual; //Flanco de subida para comenzar a escribir
	I2C_Write(estadoActual);

	estadoActual = estadoActual | (dato & 0b11110000); //Mandar dato parte alta
	I2C_Write(estadoActual);

	estadoActual = 0b11111001 & estadoActual; //Capturar dato sin perdida de informacion
	I2C_Write(estadoActual);

	I2C_Stop();
	if(tipo == 0)
		delay_us(40); //Delay para comando

	if(modo4Bits == 1)
	{
		estadoActual = estadoActual & 0b00001111;
		I2C_Write(direccion);

		estadoActual = 0b00000100 | (estadoActual & 0b00001001); //Flanco de subida para comenzar a escribir segunda parte
		I2C_Write(estadoActual);

		estadoActual = estadoActual | (dato << 4); //Mandar dato parte baja
		I2C_Write(estadoActual);

		estadoActual = 0b11111001 & estadoActual; //Capturar dato sin perdida de informacion
		I2C_Write(estadoActual);

		I2C_Stop();
		if(tipo == 0)
			delay_us(40); //Delay para comando
	}
}

//***********************************************************
static void delay_ms(unsigned int delay)
{
	unsigned int contador;
	delay = (int)(delay * 4000);
	for(contador = 0; contador<delay; contador++)
		asm nop;
}

//***********************************************************
static void delay_us(unsigned int delay)
{
	unsigned int contador;
	delay = (int)(delay * 4);
	for(contador = 0; contador<delay; contador++)
		asm nop;
}
