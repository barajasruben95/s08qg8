#include <hidef.h>
#include "derivative.h"
#include "i2c.h"

void I2C_Init(void)
{
	IICF = 0b01000000; //Baudrate de 100kbps
	IICC = 0b10010000; //Habilitar modulo I2C, sin interrupcion, modo maestro
}

void I2C_Write(unsigned char data)
{
	IICC_MST = 1; //Colocar microcontrolador en modo maestro 	
	IICD = data; //Condicion de start y transmision del dato
	do
		asm nop;
	while(IICS_IICIF == 0); //Esperar a que se complete la transmision
	I2C_ClearFlag();	
}

void I2C_Stop(void)
{
	IICC_MST = 0; //Condicion de stop colocando el microcontrolador en modo esclavo
}

void I2C_ClearFlag(void)
{
	(void) IICS; //Limpiar bandera primer paso
	IICS_IICIF = 1; //Limpiar bandera segundo paso
}
