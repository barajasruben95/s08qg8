#include <hidef.h>
#include "derivative.h"
#include "rti.h"

void RTI_Init(void)
{
	SRTISC = 0b00010111; //Interrupciones habilitadas cada 1 segundo
}

void RTI_ClearFlag(void)
{
	SRTISC_RTIACK = 1; //Apagar bandera RTI
}
