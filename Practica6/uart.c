#include <hidef.h>
#include "derivative.h"
#include "uart.h"

//Habilitar/Deshabilitar el modulo UART modo polling (lineas TX y RX)
void UART_Enable(void) { SCIC2 = 0b00001100; }
void UART_Disable(void) { SCIC2 = 0b00000000; }

//Habilitar/Deshabilitar el modulo UART TX modo polling
void UART_TX_Enable(void) { SCIC2_TE = 1; }//Habilitar solamente linea de transmision
void UART_TX_Disable(void) { SCIC2_TE = 0; }//Deshabilitar solamente linea de transmision

//Habilitar/Deshabilitar el modulo UART RX modo polling
void UART_RX_Enable(void) { SCIC2_RE = 1; }//Habilitar solamente linea de recepcion
void UART_RX_Disable(void) { SCIC2_RE = 0; }//Deshabilitar solamente linea de recepcion

//Habilitar/Deshabilitar interrupciones TX del modulo UART
void UART_InterruptTX_Enable(void) { SCIC2_TIE = 1; } //Habilitar interrupcion solo de la linea de transmision
void UART_InterruptTX_Disable(void) { SCIC2_TIE = 0; } //Deshabilitar interrupcion solo de la linea de transmision

//Habilitar/Deshabilitar interrupciones RX del modulo UART
void UART_InterruptRX_Enable(void) { SCIC2_RIE = 1; } //Habilitar interrupcion solo de la linea de recepcion
void UART_InterruptRX_Disable(void) { SCIC2_RIE = 0; } //Deshabilitar interrupcion solo de la linea de recepcion

/*
 * CONOCER SI EL BUFFER DE RECEPCION CONTIENE UN DATO DE ENTRADA
 * 	0 si no hay dato disponible
 * 	1 si hay un dato disponible 
*/
unsigned char UART_DataReady(void) { return (char) SCIS1_RDRF; }

// Leer dato de entrada y apagar bandera
unsigned char UART_Read(void)
{
	(void) SCIS1;
	return SCID;
}

//Escribir dato para enviar y limpiar bandera
void UART_Write(unsigned char data)
{
	(void) SCIS1;
	SCID = data;
}

// Seleccionar un baudrate de operacion del UART
void UART_BaudRate(unsigned char baudRate) { SCIBD = baudRate; }
