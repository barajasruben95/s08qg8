#ifndef I2C_LCD_H_
#define I2C_LCD_H_

#define _Incrementos 0b00000010
#define _Decrementos 0b00000000
#define _DisplayON 0b00000100
#define _DisplayOFF 0b00000000
#define _CursorON 0b00000010
#define _CursorOFF 0b00000000
#define _CursorBlinkingON 0b00000001
#define _CursorBlinkingOFF 0b00000000
#define _DisplayShift 0b00001000
#define _CursorMove 0b00000000
#define _ShiftCursorRigth 0b00000100
#define _ShiftCursorLeft 0b00000000
#define _8Bits 0b00010000
#define _4Bits 0b00000000
#define _2Lines 0b00001000
#define _1Line 0b00000000
#define _5x10Dots 0b00000100
#define _5x8Dots 0b00000000
#define _Linea1 0b10000000
#define _Linea2 0b11000000

//Inicializar modulo PCF8574 y I2C_LCD
void I2C_LCD_Init(unsigned char address);

//Comandos completos al I2C_LCD
void I2C_LCD_EntryMode(unsigned char incrementDecrement);
void I2C_LCD_ControlDisplay(unsigned char displayONOFF, unsigned char cursorONOFF, unsigned char blinkingONOFF);
void I2C_LCD_CursorDisplayShift(unsigned char cursorDisplayShift, unsigned char direction);
void I2C_LCD_FunctionSet(unsigned char dataLength, unsigned char displayLines, unsigned char characterFont);
void I2C_LCD_SetDDRAM(unsigned char posicion, unsigned char lineNumber);

//Comandos resumidos al I2C_LCD
void I2C_LCD_Clear(void);
void I2C_LCD_Home(void);
void I2C_LCD_4Bits2Lines5x8(void);
void I2C_LCD_IncrementarCursor(void);
void I2C_LCD_Linea1(void);
void I2C_LCD_Linea2 (void);
void I2C_LCD_PosicionLinea1(unsigned char posicion);
void I2C_LCD_PosicionLinea2(unsigned char posicion);
void I2C_LCD_OFF(void);
void I2C_LCD_CursorON(void);
void I2C_LCD_CursorOFF(void);

//Escritura de datos
void I2C_LCD_Write(unsigned char character);

//Funciones estaticas
static void I2C_LCD_Inicializar(void);
static void I2C_LCD_Escribir(unsigned char dato, unsigned char tipo);
static void delay_ms(unsigned int delay);
static void delay_us(unsigned int delay);

#endif
