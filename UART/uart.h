#ifndef UART_H_
#define UART_H_

//BaudRate = BusCLK/(SBR12:SBR0 * 16)
#define tasa110bps 2273
#define tasa300bps 833
#define tasa1200bps 208
#define tasa2400bps 104
#define tasa4800bps 52
#define tasa9600bps 26
#define tasa19200bps 13
#define tasa38400bps 6
#define tasa57600bps 4
#define tasa115200bps 2
#define tasa230400bps 1

void UART_Enable(void);
void UART_Disable(void);

void UART_TX_Enable(void);
void UART_TX_Disable(void);
void UART_RX_Enable(void);
void UART_RX_Disable(void);

void UART_InterruptTX_Enable(void);
void UART_InterruptTX_Disable(void);
void UART_InterruptRX_Enable(void);
void UART_InterruptRX_Disable(void);

unsigned char UART_DataReady(void);

void UART_BaudRate(unsigned char baudRate);
void UART_ClearFlag(void);

unsigned char UART_Read(void);
void UART_Write(unsigned char data);

#endif
