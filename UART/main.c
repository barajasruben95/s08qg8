#include <hidef.h>
#include "derivative.h"
#include "uart.h"

unsigned char str[] = ("\n\r");
unsigned char i = 0;
unsigned char numero = 0;
unsigned char banderaFinRX = 0; 
unsigned char banderaComienzoTX = 0;

interrupt 15 void RX_ISR(void)
{
	UART_ClearFlag();
	
	if((UART_Read()>'/') && (UART_Read()<':'))
		numero = (numero * 10) + UART_Read() - '0';
	if(UART_Read()==13)
	{
		banderaFinRX = 1;
		UART_InterruptRX_Disable();
	}
}

interrupt 16 void TX_ISR(void)
{
	UART_ClearFlag();
	UART_Write(str[i++]);
	if(str[i] == 0)
	{
		UART_InterruptTX_Disable();
	}
}

void sendMsg(void)
{
	UART_Write(numero + '0');
	UART_InterruptTX_Enable();
}

void main(void)
{	
	SOPT1 = 0x52; //Desactivar watchdog
	UART_BaudRate(tasa9600bps);
	UART_Enable();
	UART_InterruptRX_Enable();
	
	EnableInterrupts;
	
	for (;;)
	{
		if(banderaFinRX == 1)
		{
			banderaFinRX = 0;
			banderaComienzoTX = 1;
		}
		
		if(banderaComienzoTX == 1)
		{
			banderaComienzoTX = 0;
			sendMsg();
		}
	}
}
