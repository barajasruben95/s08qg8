/*
 * MOTOR A PASOS
 *  - 1000 rpm
 *  - Secuencia: rojo, azul, cafe, amarillo.
 *  - Com�n: blanco 
 */

#include <hidef.h>
#include "derivative.h"

unsigned char secuencia[] = { 0b00001000, 0b00001100, 0b00000100, 0b00000110, 0b00000010, 0b00000011, 0b00000001, 0b00001001 };
unsigned int datoADC = 0;

void conversionADC(void)
{
	ADCSC1 = 0x00; //Iniciar conversion ADC modo no continuo por el canal 0 sin interrupciones.
	do
	{
		asm nop;
	} while (ADCSC1_COCO == 0);	//Esperar hasta que la conversion est� lista.
	datoADC = ADCRL; //Leer el dato del ADC. Se apagar� la bandera tambi�n.
}

void delay(unsigned tiempo)
{
	MTIMSC = 0b0000000; //Interrupcion deshabilitada, conteo desde cero y activado.
	do
	{
		asm nop;
	} while (MTIMSC_TOF == 0);	//Esperar hasta que pase el tiempo
	MTIMMOD = 156; //Apagar bandera
	MTIMSC = 0b00010000;
}

/*
void delay(unsigned tiempo)
{
	TPMSC = 0x03;
	TPMMOD = tiempo;
	TPMSC_CLKSA = 1; //Activo el timer con clock igual a Bus clock (4MHz).
	while (!TPMSC_TOF); //Mientras el bit de overflow es 0, espera
}
*/


void main(void)
{
	unsigned char i = 0;

	//Configuraciones generales
	SOPT1 = 0x52; //Desactivar watchdog

	//Configuraciones de puertos
	PTBDD = 0x0F; //Poner en salida PTB4 a PTB0	
	PTAPE_PTAPE2 = 1; //Activar pull up en PA2
	APCTL1_ADPC0 = 1; //Configurar como entrada anal�gica el canal 0 (PTA0)

	//Configuraciones del TIMER
	MTIMCLK = 0b00000100; //Fuente del reloj interno con preescalador en 16 (4ms)
	MTIMMOD = 250;
	//MTIMMOD = 2;
	
	for (;;)
	{
		conversionADC();

		if (PTAD_PTAD2 == 1) //SECUENCIA SIN PRESIONAR PUSH BOTTON
		{
			if (i == 8)
				i = 0;
			PTBD = secuencia[i++];
		}
		else //SECUENCIA PRESIONANDO EL PUSH BOTTON
		{
			if (i == 255)
				i = 7;
			PTBD = secuencia[i--];
		}
		delay(datoADC);
	}
}
